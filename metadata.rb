name 'chef'
maintainer 'Jayashree'
maintainer_email 'jayashree.bose01@gmail.com'
license 'All Rights Reserved'
description 'Installs/Configures chef'
long_description 'Installs/Configures chef'
version '0.1.2'
chef_version '>= 13.0'
source_url 'https://gitlab.com/chef_project/chef'
issues_url 'https://gitlab.com/chef_project/chef/issues'
supports 'ubuntu'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
