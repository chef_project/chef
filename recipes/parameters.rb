#
# Cookbook:: chef
# Recipe:: parameters
#
# Copyright:: 2019, The Authors, All Rights Reserved.
require 'chef/provisioning/azurerm'
with_driver 'AzureRM:ff243536-d539-4ffc-a501-e0c4df20264b'

azure_resource_template 'my-deployment10' do
  resource_group 'myresourcegroup'
  template_source "#{Chef::Config[:cookbook_path]}/chef/files/default/azuredeploy.json"
  parameters  dnsLabelPrefix: 'testnode1-pcloud',
              adminUsername: 'jayashree',
              adminPassword: 'Hulogujibaba@23',
              vmName: 'myNode1',
              nicName: 'myNode1Nic',
              publicIPAddressName: 'myNode1IP'
end
